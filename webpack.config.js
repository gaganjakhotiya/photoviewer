var webpack = require('webpack');
module.exports = {
    entry: "./src/index.tsx",
    output: {
        publicPath: "/dist/",
        path: "./dist/",
        filename: "bundle.js"
    },
    devtool: "source-map",
    resolve: {
    	extensions: ['', '.js', '.jsx', '.tsx', '.ts']
    },
    plugins: [
        //new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
    	loaders: [
            { test: /\.jsx$/, loader: 'jsx-loader'},
    		{ test: /\.tsx$/, loader: 'typescript-simple-loader'},
            { test: /\.css$/, loader: 'style!css' }
    	]
    }
};