/// <reference path='../typings/__definitions.d.ts' />

import ReactDOM = require('react-dom');
import React = require('react');
import App = require('./App');
import Viewer = require('./Viewer');

ReactDOM.render(<App/>, document.getElementById('content'));