/// <reference path='../typings/__definitions.d.ts' />

import Viewer = require('./Viewer');
import React = require('react/addons');
import ReactDOM = require('react-dom');

interface AppState{
  photoIndex?: number;
  valueIndex?: number;
}

interface AppProps {}

class App extends React.Component<AppProps, AppState>{
  static defaultProps = {};
  static initialState = {
    photoIndex : 0,
    valueIndex: 1
  };
  
  constructor() {
    super();
    this.render = this.render.bind(this);
    this.state = Object.create(App.initialState);
  }

  handleChange(addend: number) {
    var newIndex = this.state.photoIndex + addend;
    var maxPhotos = 3;
    newIndex = Math.max(0, Math.min(maxPhotos - 1, newIndex));

    this.setState({
      photoIndex: newIndex,
      valueIndex: newIndex+1
    })
  }

  handleKeyPress(e: any) {
    if(!isNaN(e.key)){
      this.setState({
        valueIndex: e.key
      })
    }else if (e.key == 'Enter' && this.state.valueIndex > 0 && this.state.valueIndex < 4) {
      this.setState({
        photoIndex: this.state.valueIndex-1
      })
    }
  }

  render() {
    return (
      <div className="app">
        <div className="title"><h1>Viewer</h1></div>
        <Viewer index={this.state.photoIndex} />
          <div className="controls">
            <button disabled={this.state.photoIndex == 0} onClick={this.handleChange.bind(this, -1) }>Previous</button>
            <input type="text" value={(this.state.valueIndex).toString()} onKeyPress={this.handleKeyPress.bind(this)} />
            <button disabled={this.state.photoIndex == 2} onClick={this.handleChange.bind(this, 1) }>Next</button>
          </div>
        </div>
    );
  }
}

export = App;