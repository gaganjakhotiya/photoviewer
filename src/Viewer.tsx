/// <reference path='../typings/__definitions.d.ts' />

import React = require('react/addons');

interface ViewerProps {
  index: number;
}

class Viewer extends React.Component<ViewerProps, any>{
  static defaultProps = {
    index: 0
  };

  constructor(props: ViewerProps){
    super(props);
    this.props = props;
  }
  
  render() {
    return (
      <div className="photos">
  		  <React.addons.CSSTransitionGroup transitionName="photo">
  			 <img key={this.props.index} src={"photos/"+this.props.index+".jpg"} />
  		  </React.addons.CSSTransitionGroup>
      </div>
  	);
  }
}

export = Viewer;